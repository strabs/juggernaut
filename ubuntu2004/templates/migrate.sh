#!/bin/bash

DOTENV="/home/{{ user }}/webapps/{{ site.primary_domain }}/sysadmin/{{ site.app_name }}.env"
# Load .env
if [ -f $DOTENV ]; then
    # Load Environment Variables
    export $(cat $DOTENV | grep -v '#' | awk '/=/ {print $1}')
fi

cd /home/{{ user }}/webapps/{{ site.primary_domain }}/
pipenv run python /home/{{ user }}/webapps/{{ site.primary_domain }}/deployments/current/manage.py migrate

