#!/usr/bin/env python3
"""Providing a richer backup system than just every hour for 4 days.


In this file, we attempt to provide backups as follows:

    1 backup per hour for 24 hours.
    1 backup per day for 7 days
    1 backup per week for 4 weeks
    1 backup per month for 6 months.
"""
import subprocess
import datetime
import itertools
import os

DEBUG=False

BASE_DIR="{{ backup_directory }}/{{ item.name }}"

def month_mod(m):
    return ((m-1)%12)+1

def test_monthly_backups():
    now = datetime.datetime.now().replace(day=1)
    last_12_months = [now.replace(month=month_mod(m)) for m in range(now.month, month_mod(now.month-12), -1)]
    return "\n".join([
        d.strftime(f"{BASE_DIR}/monthly/{{ item.name }}-%Y-%m-%d-%H%M.sql.bz2")
        for d in last_12_months
    ])

def test_weekly_backups():
    now = datetime.datetime.now()
    last_12_weeks = [now - datetime.timedelta(days=7*i) for i in range(0, 12)]
    return "\n".join([
        d.strftime(f"{BASE_DIR}/weekly/{{ item.name }}-%Y-%m-%d-%H%M.sql.bz2")
        for d in last_12_weeks
    ])

def test_daily_backups():
    now = datetime.datetime.now()
    last_60_days = [now - datetime.timedelta(days=i) for i in range(0, 60)]
    return "\n".join([
        d.strftime(f"{BASE_DIR}/daily/{{ item.name }}-%Y-%m-%d-%H%M.sql.bz2")
        for d in last_60_days
    ])

def test_hourly_backups():
    now = datetime.datetime.now()
    last_60_days = [now - datetime.timedelta(hour=i) for i in range(0, 24*60)]
    return "\n".join([
        d.strftime(f"{BASE_DIR}/hourly/{{ item.name }}-%Y-%m-%d-%H%M.sql.bz2")
        for d in last_60_days
    ] + [
        f"{BASE_DIR}/hourly/latest.sql.bz2"
    ])



#-------------------------------------------------------------------------------
def run(command):
    if DEBUG:
        print(command)
    else:
        result = subprocess.run(command.split(' '), stdout=subprocess.PIPE)
        return result.stdout.decode('utf-8')

#-------------------------------------------------------------------------------
def log(msg):
    if not DEBUG:
        return
    print(msg)


#-------------------------------------------------------------------------------
def find_backups(target_directory, pattern="*.sql.bz2"):
    """Returns the set of backups from a given directory that match a pattern.
    """
    return run("find %s -name %s" % (target_directory, pattern))

#-------------------------------------------------------------------------------
def parse_backups(backup_string, date_format="%Y-%m-%d-%H%M"):
    """Use this to parse the output of a find command.

    returns a sorted tuple of datetime objects and paths.
    """
    potential_paths = [path for path in backup_string.split("\n") if path]
    paths = []
    for path in potential_paths:
        try:
            backup_time = datetime.datetime.strptime(path, date_format)
            paths.append((backup_time, path))
        except ValueError:
            continue
    return sorted(paths)


#-------------------------------------------------------------------------------
def monthly_cutoff(months_ago):
    now = datetime.datetime.now()
    start_of_month = now.replace(day=1, hour=0, minute=0)
    return (start_of_month - datetime.timedelta(days=28*(months_ago-1))).replace(day=1, hour=0, minute=0, second=0, microsecond=0)

#-------------------------------------------------------------------------------
def weekly_cutoff(weeks_ago):
    now = datetime.datetime.now()
    start_of_week = now
    while start_of_week.weekday() != 0:
        start_of_week -= datetime.timedelta(days=1)

    return start_of_week - datetime.timedelta(weeks=weeks_ago)

#-------------------------------------------------------------------------------
def daily_cutoff(days_ago):
    now = datetime.datetime.now()
    return now - datetime.timedelta(days=days_ago)

#-------------------------------------------------------------------------------
def hourly_cutoff(hours_ago):
    now = datetime.datetime.now()
    return now - datetime.timedelta(hours=hours_ago)

#-------------------------------------------------------------------------------
def beginning_of_month():
    now = datetime.datetime.now()
    return now.replace(day=1, hour=0, minute=0, second=0, microsecond=0)

#-------------------------------------------------------------------------------
def beginning_of_week():
    now = datetime.datetime.now()
    start_of_week = now
    while start_of_week.weekday() != 0:
        start_of_week -= datetime.timedelta(days=1)
    return start_of_week.replace(hour=0, minute=0, second=0, microsecond=0)

#-------------------------------------------------------------------------------
def beginning_of_day():
    now = datetime.datetime.now()
    return now.replace(hour=0, minute=0, second=0, microsecond=0)

#-------------------------------------------------------------------------------
def remove_backups(files, cutoff_time):
    """Use this tool to remove backups older than given input from a directory.
    """
    def older_than(item):
        item_time, item = item
        return item_time < cutoff_time
    files_to_remove = filter(older_than, files)
    for item in files_to_remove:
        date_time, file_path = item
        run("rm %s" % file_path)

#-------------------------------------------------------------------------------
def add_to_backups(current_files, potential_files, cutoff_time, target_dir):
    """Copies the appropriate file into this backup rotation.
    """
    # First figure out if we have a backup in the current rotation that's after
    # the cutoff time. If not, we will try and find one in the potential files.
    for backup_time, backup in current_files:
        if backup_time >= cutoff_time:
            return

    # If we get here, none of the backups are appropriate
    for backup_time, backup in potential_files:
        if backup_time >= cutoff_time:
            run("cp %s %s" % (backup, target_dir))
            return

#-------------------------------------------------------------------------------
def main():
    base_format = f"{BASE_DIR}/%s/{{ item.name }}-%%Y-%%m-%%d-%%H%%M.sql.bz2"
    hourly_format = base_format % "hourly"
    daily_format = base_format % "daily"
    weekly_format = base_format % "weekly"
    monthly_format = base_format % "monthly"

    base_directory = f"{BASE_DIR}/%s/"
    hourly_directory = base_directory % "hourly"
    daily_directory = base_directory % "daily"
    weekly_directory = base_directory % "weekly"
    monthly_directory = base_directory % "monthly"

    if DEBUG:
        hourly_find_output = test_hourly_backups()
        daily_find_output = test_daily_backups()
        weekly_find_output = test_weekly_backups()
        monthly_find_output = test_monthly_backups()
    else:
        hourly_find_output = find_backups(hourly_directory)
        daily_find_output = find_backups(daily_directory)
        weekly_find_output = find_backups(weekly_directory)
        monthly_find_output = find_backups(monthly_directory)

    hourly_backups = parse_backups(hourly_find_output, date_format=hourly_format)
    daily_backups = parse_backups(daily_find_output, date_format=daily_format)
    weekly_backups = parse_backups(weekly_find_output, date_format=weekly_format)
    monthly_backups = parse_backups(monthly_find_output, date_format=monthly_format)

    log("Monthly")
    remove_backups(monthly_backups, monthly_cutoff({{ site.sysadmin.rolling_backup.monthly|default(12) }}))
    log(beginning_of_month())
    add_to_backups(
            monthly_backups,
            hourly_backups,
            beginning_of_month(),
            f"{BASE_DIR}/monthly/",
        )

    log("weekly")
    log(beginning_of_week())
    remove_backups(weekly_backups, weekly_cutoff({{ site.sysadmin.rolling_backup.weekly|default(8) }}))
    add_to_backups(
            weekly_backups,
            hourly_backups,
            beginning_of_week(),
            f"{BASE_DIR}/weekly/",
        )

    log("daily")
    log(beginning_of_day())
    remove_backups(daily_backups, daily_cutoff({{ site.sysadmin.rolling_backup.daily|default(14) }}))
    add_to_backups(
            daily_backups,
            hourly_backups,
            beginning_of_day(),
            f"{BASE_DIR}/daily/"
        )

    log("hourly")
    remove_backups(hourly_backups, hourly_cutoff({{ site.sysadmin.rolling_backup.hourly|default(120) }}))

    #print parse_backups(test_hourly_backups, date_format=hourly_format)

if __name__ == "__main__":
    main()
