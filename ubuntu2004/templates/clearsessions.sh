#!/bin/bash
cd /home/{{ user }}/webapps/{{ site.primary_domain }}/
/usr/local/bin/pipenv run python /home/{{ user }}/webapps/{{ site.primary_domain }}/deployments/current/manage.py clearsessions
