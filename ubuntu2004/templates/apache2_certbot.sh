#!/bin/bash
test -d /etc/letsencrypt/live/{{ site.primary_domain }} && mv /etc/letsencrypt/live/{{ site.primary_domain }} /etc/letsencrypt/live/{{ site.primary_domain }}.aside
test -d /etc/letsencrypt/archive/{{ site.primary_domain }} && mv /etc/letsencrypt/archive/{{ site.primary_domain }} /etc/letsencrypt/archive/{{ site.primary_domain }}.aside
test -d /etc/letsencrypt/renewal/{{ site.primary_domain }}.conf && mv /etc/letsencrypt/renewal/{{ site.primary_domain }}.conf /etc/letsencrypt/renewal/{{ site.primary_domain }}.conf.aside

certbot certonly --apache \
    -d {{ site.primary_domain }} \
    {% for domain in site.other_domains %} -d {{ domain }} {% endfor %} \
    {% for domain in site.other_primary_domains %} -d {{ domain }} {% endfor %} \
    --force-renewal \
    --no-redirect \
    --non-interactive \
    --quiet \
    --agree-tos \
    -m {{ site.certificate.letsencrypt_email }}
