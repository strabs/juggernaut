import datetime
import hashlib
import hmac
import os
import subprocess

from flask import Flask, request, abort

application = Flask(__name__)

WEBHOOK_SECRET = "{{ site.github.webhook_secret }}"

def get_output(args):
    return subprocess.check_output(args).decode("utf-8")

def run(args):
    print(f'GITHUB-RUN: {" ".join(args)}')
    subprocess.check_output(args)

@application.route('/github', methods=['POST'])
def deploy():
    if "X-Hub-Signature" not in request.headers:
        abort(400) # bad request if no header present

    signature = request.headers['X-Hub-Signature']
    payload = request.data

    secret = WEBHOOK_SECRET.encode() # must be encoded to a byte array

    # contruct hmac generator with our secret as key, and SHA-1 as the hashing function
    hmac_gen = hmac.new(secret, payload, hashlib.sha1)

    # create the hex digest and append prefix to match the GitHub request format
    digest = "sha1=" + hmac_gen.hexdigest()

    if not hmac.compare_digest(digest, signature):
        abort(400) # if the signatures don't match, bad request not from GitHub

    return deploy_hugo()

@application.route('/hugo', methods=['GET'])
def hugo():
    return get_output(['/snap/bin/hugo', 'version'])
# Show the hugo version.

@application.route('/pubkey', methods=['GET'])
def pubkey():
    return open("/home/{{ user }}/.ssh/id_ed25519.pub").read()

def git_cmd(args=None):
    args = args or []
    repo_name = "{{ site.github.repo_name }}"
    git_dir = f"/home/{{ user }}/tmp/{repo_name}/.git"
    git_work_tree = f"/home/{{ user }}/tmp/{repo_name}"
    git_base = ["/usr/bin/git", f"--git-dir={git_dir}", f"--work-tree={git_work_tree}"]
    return git_base + args

def hugo_cmd(args=None):
    args = args or []
    hugo = "/snap/bin/hugo"
    hugo_base = [hugo, "--cacheDir=/home/{{ user }}/tmp/hugo_cache", "--environment", "{{ site.hugo.environment }}"]
    return hugo_base + args

def deploy_hugo():
    repo_url = "{{ site.github.repo_url }}"
    repo_name = "{{ site.github.repo_name }}"
    git_work_tree = f"/home/{{ user }}/tmp/{repo_name}"

    date_dirname = datetime.datetime.now().strftime("%Y-%m-%d-%H%M%S")
    deployment_target = f"/home/{{ user }}/webapps/{{ site.primary_domain }}/deployment/{date_dirname}"
    build_source = f"/home/{{ user }}/tmp/{repo_name}/public"
    link_target = f"/home/{{ user }}/webapps/{{ site.primary_domain }}/deployment/htdocs"

    # Step 1, create base directories
    run(["/usr/bin/mkdir", "-p", "/home/{{ user }}/tmp/hugo_cache"])
    git_checkout_dir = f"/home/{{ user }}/tmp/{repo_name}"

    # Step 2 do an initial checkout if it doesn't exist.
    if not os.path.exists(git_checkout_dir):
        run(["/usr/bin/git", "clone", repo_url, git_work_tree])

    # Step 3 clean public
    os.chdir(git_work_tree)
    run(["/usr/bin/rm", "-rf", build_source])

    # Step 4 reset git
    run(git_cmd(["reset", "--hard"]))

    # Step 5 pull latest
    run(git_cmd(["pull", "origin", "master"]))

    # Step 6 pull latest
    run(git_cmd(["submodule", "update", "--init", "--recursive"]))

    # Step 7 build hugo
    run(hugo_cmd())

    # Step 8 copy to output directory
    run(["/usr/bin/cp", "-r", build_source, deployment_target])

    # Step 8 copy to output directory
    run(["/usr/bin/ln", "-sfn", deployment_target, link_target])

    return "success"
