#!/bin/bash
cd /home/{{ user }}/webapps/{{ primary_domain }}/
pipenv run python /home/{{ user }}/webapps/{{ primary_domain }}/deployments/current/manage.py migrate

