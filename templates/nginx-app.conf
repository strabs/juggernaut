
    # Allow for a reasonable upload size.
    client_max_body_size 25M;

    # path for static files
    root /home/{{ user }}/webapps/{{ primary_domain }}/deployments/current/htdocs;

    location / {
        # check for the presence of a maintenance page.
        if (-f /home/{{ user }}/webapps/{{ primary_domain }}/sysadmin/maintenance_on.html) {
            return 503;
        }

        # checks for static file, if not found proxy to app
        try_files $uri @proxy_to_app;
        expires 7d;
    }

    location /content {
        root {% if content_root is defined %}{{ content_root }}{% else %}/home/{{ user }}/webapps/{{ primary_domain }}/content{% endif %};

        if ($request_uri ~* ".(ico|gif|jpe?g|png|tiff)$") {
            expires 7d;
        }
    }

    location @proxy_to_app {
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header Host $http_host;
        # we don't want nginx trying to do something clever with
        # redirects, we set the Host: header above already.
        proxy_redirect off;
        proxy_pass http://{{ app_name }};
    }

    error_page 503 /maintenance_on.html;
    location = /maintenance_on.html {
        root /home/{{ user }}/webapps/{{ primary_domain }}/sysadmin;
    }

    error_page 500 502 504 /500.html;
    location = /500.html {
        root /home/{{ user }}/webapps/{{ primary_domain }}/htdocs;
    }

    {% for extra_include in nginx.extra_includes %}
        include {{ extra_include }};
    {% endfor %}

