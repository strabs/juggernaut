#!/bin/bash
test -d /etc/letsencrypt/live/{{ primary_domain }} && mv /etc/letsencrypt/live/{{ primary_domain }} /etc/letsencrypt/live/{{ primary_domain }}.aside
certbot --nginx \
    -d {{ primary_domain }} \
    {% for domain in other_domains %} -d {{ domain }} {% endfor %} \
    {% for domain in other_primary_domains %} -d {{ domain }} {% endfor %} \
    --force-renewal \
    --no-redirect \
    --non-interactive \
    --quiet \
    --agree-tos \
    -m {{ certificate.letsencrypt_email }}
