#!/bin/bash

# crontab entry:
# * * * * * command-to-be-executed
# 15 * * * * /home/{{ user }}/webapps/{{ primary_domain }}/sysadmin/backup.sh
OF=/home/{{ user }}/backups/{{ primary_domain }}/{{ item.name }}/hourly/{{ item.name }}-`date +%Y-%m-%d-%H%M`.sql
BZOF=/home/{{ user }}/backups/{{ primary_domain }}/{{ item.name }}/hourly/{{ item.name }}-`date +%Y-%m-%d-%H%M`.sql.bz2
pg_dump --clean --no-owner --no-privileges --format=plain --host localhost {{ item.name }} --username {{ item.user }} >& $OF 
bzip2 $OF 
ln -sf $BZOF /home/{{ user }}/backups/{{ primary_domain }}/latest.sql.bz2
