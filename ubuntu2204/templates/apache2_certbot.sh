#!/bin/bash
test -d /etc/letsencrypt/live/{{ primary_domain }} && mv /etc/letsencrypt/live/{{ primary_domain }} /etc/letsencrypt/live/{{ primary_domain }}.aside
test -d /etc/letsencrypt/archive/{{ primary_domain }} && mv /etc/letsencrypt/archive/{{ primary_domain }} /etc/letsencrypt/archive/{{ primary_domain }}.aside
test -d /etc/letsencrypt/renewal/{{ primary_domain }}.conf && mv /etc/letsencrypt/renewal/{{ primary_domain }}.conf /etc/letsencrypt/renewal/{{ primary_domain }}.conf.aside

certbot certonly --apache \
    -d {{ primary_domain }} \
    {% for domain in other_domains %} -d {{ domain }} {% endfor %} \
    {% for domain in other_primary_domains %} -d {{ domain }} {% endfor %} \
    --force-renewal \
    --no-redirect \
    --non-interactive \
    --quiet \
    --agree-tos \
    -m {{ certificate.letsencrypt_email }}
