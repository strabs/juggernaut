#!/bin/bash

DOTENV="/home/{{ user }}/webapps/{{ primary_domain }}/sysadmin/{{ app_name }}.env"
# Load .env
if [ -f $DOTENV ]; then
    # Load Environment Variables
    export $(cat $DOTENV | grep -v '#' | awk '/=/ {print $1}')
fi

cd /home/{{ user }}/webapps/{{ primary_domain }}/
source .venv/bin/activate
pipenv run python /home/{{ user }}/webapps/{{ primary_domain }}/deployments/current/manage.py migrate

