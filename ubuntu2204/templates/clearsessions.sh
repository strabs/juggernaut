#!/bin/bash
cd /home/{{ user }}/webapps/{{ primary_domain }}/
/usr/local/bin/pipenv run python /home/{{ user }}/webapps/{{ primary_domain }}/deployments/current/manage.py clearsessions
