#!/bin/bash
cd /home/{{ user }}/webapps/{{ primary_domain }}
source .venv/bin/activate
pipenv sync
