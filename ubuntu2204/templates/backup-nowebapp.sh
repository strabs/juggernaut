#!/bin/bash

# crontab entry:
# * * * * * command-to-be-executed
# 15 * * * * {{ backup_directory }}{{ item.name }}/backup.sh
OF={{ backup_directory }}/{{ item.name }}/hourly/{{ item.name }}-`date +%Y-%m-%d-%H%M`.sql
BZOF={{ backup_directory }}/{{ item.name }}/hourly/{{ item.name }}-`date +%Y-%m-%d-%H%M`.sql.bz2
pg_dump --clean --no-owner --no-privileges --format=plain --host {{ item.host|default('localhost') }} --port {{ item.port|default(5432) }} {{ item.name }} --username {{ item.user }} >& $OF 
bzip2 $OF 
ln -sf $BZOF {{ backup_directory }}/{{ item.name }}/latest.sql.bz2
